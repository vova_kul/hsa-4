# HSA-4 Homework project for siege load testing

This an HSA-4 Homework project to show siege load testing.

Load testing results

Concurrency 10

| Caching            | Availability     | Avg. response time | Throughput  | Transaction rate |
|--------------------|------------------|--------------------|-------------|------------------|
| No cache           | 100%             | 0.97 secs          | 0.00 MB/sec | 10.25 trans/sec  |
| Simple cache       | 100%             | 0.04 secs          | 0.01 MB/sec | 258.72 trans/sec |
| Probalistic cache  | 100%             | 0.04 secs          | 0.01 MB/sec | 229.34 trans/sec |

Concurrency 25

| Caching            | Availability     | Avg. response time | Throughput  | Transaction rate |
|--------------------|------------------|--------------------|-------------|------------------|
| No cache           | 100%             | 2.49 secs          | 0.00 MB/sec | 9.81 trans/sec   |
| Simple cache       | 100%             | 0.10 secs          | 0.01 MB/sec | 250.44 trans/sec |
| Probalistic cache  | 100%             | 0.10 secs          | 0.01 MB/sec | 245.97 trans/sec |

Concurrency 50

| Caching            | Availability     | Avg. response time | Throughput  | Transaction rate |
|--------------------|------------------|--------------------|-------------|------------------|
| No cache           | 100%             | 4.76 secs          | 0.00 MB/sec | 10.06 trans/sec  |
| Simple cache       | 100%             | 0.21 secs          | 0.01 MB/sec | 240.89 trans/sec |
| Probalistic cache  | 100%             | 0.19 secs          | 0.01 MB/sec | 258.53 trans/sec |

Concurrency 100

| Caching            | Availability     | Avg. response time | Throughput  | Transaction rate |
|--------------------|------------------|--------------------|-------------|------------------|
| No cache           | 100%             | 9.01 secs          | 0.00 MB/sec | 10.10 trans/sec  |
| Simple cache       | 100%             | 0.37 secs          | 0.01 MB/sec | 267.08 trans/sec |
| Probalistic cache  | 100%             | 0.46 secs          | 0.01 MB/sec | 214.19 trans/sec |

Grafana PostgreSQL load

![Grafana PostgreSQL load](./screens/Grafana.png?raw=true "Grafana PostgreSQL load")

Inserts

| Caching            | Availability     | Avg. response time | Throughput  | Transaction rate |
|--------------------|------------------|--------------------|-------------|------------------|
| Concurrency 10     | 100%             | 0.02 secs          | 0.02 MB/sec | 404.38 trans/sec |
| Concurrency 25     | 100%             | 0.07 secs          | 0.01 MB/sec | 367.57 trans/sec |
| Concurrency 50     | 100%             | 0.13 secs          | 0.01 MB/sec | 374.82 trans/sec |
| Concurrency 100    | 100%             | 0.22 secs          | 0.01 MB/sec | 372.38 trans/sec |


## To reproduce results

Create a new database

```sql
CREATE DATABASE test_perf;
CREATE USER test_perf_user WITH encrypted password 'test_pass';
GRANT ALL PRIVILEGES ON DATABASE test_perf TO test_perf_user;
```

Create a schema from schema.sql

```bash
$ psql -U test_perf_user -d test_perf -a -q -f "/path/to/schema.sql"
```

Generate test data

```bash
$ python gen_test_data.py
```

Load test data into the database

```bash
$ psql -U test_perf_user -d test_perf -W
```

```sql
\copy test FROM 'test_data.csv' DELIMITER ',' CSV HEADER;
```

Run the app

```bash
$ gunicorn -w 4 --bind :5000 app:app
```

Run siege load testing (for example, probalistic cache)

```bash
$ siege --benchmark --concurrent=50 --time=1M --file=siege_urls_probabilistic_cache 
```

Run siege load testing for insert
```bash
$ siege --benchmark --concurrent=10 --time=1M --content-type "application/json" --file=siege_urls_insert
```